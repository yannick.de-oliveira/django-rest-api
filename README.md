# API REST Django

## Mise en place de l'environnement
Ces étapes seront déjà réalisées dans la VM.<br>

Installer virtualenv et éventuellement pip s'ils ne sont pas déjà installés :<br>
`sudo apt-get install python3-venv`<br>
`sudo apt-get install python3-pip`<br>

Créer un environnement virtuel pour python :<br>
`python3 -m venv django-vuejs`<br>

Activer l'environnement virtuel :<br>
`source django-vuejs/bin/activate`<br>

Se déplacer dans le répertoire contenant le code source de l'api :<br>
`cd /mon/repository/git/django-rest-api/`<br>

Installer les dépendances :<br>
`pip install -r requirements.txt`

## Installer la base de données

Pour l'atelier il sera peut être plus simple d'utiiser SQLite. Pour créer la base de données il suffit d'executer la commande suivante<br>
`python manage.py migrate`<br>
`python manage.py shell < data_loader.py`

## Démarrer le serveur
`python manage.py runserver`<br>