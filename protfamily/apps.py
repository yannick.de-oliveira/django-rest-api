from django.apps import AppConfig


class ProtfamilyConfig(AppConfig):
    name = 'protfamily'
