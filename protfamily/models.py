from django.db import models
from django.db.models.deletion import CASCADE


# Create your models here.
class Family(models.Model):
    name = models.CharField("Family name", max_length=150)
    
    def __str__(self):
        return self.name

class Alignment(models.Model):
    seq_id = models.CharField("Id", max_length=250)
    seq = models.TextField("Sequence")
    family = models.ForeignKey('Family', on_delete=CASCADE)
    def __str__(self):
        return self.seq_id

class Stat(models.Model):
    proteins = models.TextField("Proteins")
    species = models.TextField("Species")
    family = models.ForeignKey('Family', on_delete=CASCADE)
    
    def __str__(self):
        return self.family.name
        
class Tree(models.Model):
    tree = models.TextField("Tree")
    family = models.ForeignKey('Family', on_delete=CASCADE)
    def __str__(self):
        return self.family.name
    